VisionLabs LUNA CARS Analytics
=================================

Introduction
----------------

This document provides a general description of the LUNA CARS.Analytics version 2.0.9.

The document is intended to explain to the user basic functionality of the system for collecting and analyzing detection events and recognition of the vehicle and the license plate.

General information
---------------------

VisionLabs LUNA CARS is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS.Analytics, CARS.API and CARS.Stream.

VisionLabs CARS.Analytics designed for detection and tracking of vehicles and license plates in a video stream or detection in images. The main functions of the service are presented below:

* Display of vehicle and license plate detection events;

* Setting up lists for creating incidents;

* Display of incidents;

* Search for events by incidents;

* Management of user accounts and their access rights;

* Viewing processed video streams from cameras;

* Creation of tasks for search by image and export of search results to a «.xlsx» file.

Service Description
------------------------

Users interact with the system via web-browser.

CARS.Analytics is supported in the following browser versions:

* Microsoft Edge (44.0 or newer);

* Mozilla Firefox (60.3.0 or newer);

* Google Chrome (50.0 or newer).

The general scheme of operation of LUNA CARS products is shown in Figure 1.
