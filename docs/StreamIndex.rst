VisionLabs LUNA CARS Stream
=============================

Introduction
-------------------

This document contains general description of the LUNA CARS.Stream 1.0.6 service.

The document is intended to explain to the user basic functionality of the service for detecting and tracking vehicle and license plate.

General information
--------------------

VisionLabs LUNA CARS is a system designed to recognize vehicle and license plate attributes. The system consists of three services: CARS.Analytics, CARS.API and CARS.Stream.

VisionLabs CARS.Stream designed for detection and tracking of vehicles and LP in a video stream or detection in images. The main functions of the service are presented below:

* Video stream processing;

* Detection and tracking of vehicles and license plates;

* Choosing the best shot;

* Display of results of detection and recognition.

System description of CARS Stream
----------------------------------

CARS Stream processes the incoming video stream and searches for vehicles and license plate. The best shot is selected for each detected vehicle, after which the image of the vehicle and its license plate are sent to CARS.API for recognition.

The general scheme of CARS.Stream operation is shown in Figure 1.
