Примечания к выпуску
=====================

Версия 0.0.12
--------------

* Добавлен классификатор «grz_ai_recognition», который реализует функционал классификатора «grz_all_countries» без вывода страны.  

* Исправлены файл «modelRunner.cfg»: теперь для всех стран, кроме ОАЭ поддерживает и латиница, и кириллица.  

* Добавлен классификатор «vehicle_ermergency_type». 

* Добавлена проверка лицензии. 

Версия 0.0.11
--------------

* Добавлена поддержка распознавания ГРЗ ОАЭ. Включение происходит через конфигурационный файл «modelRunner.cfg»; 

* Добавлены классификаторы «uae_recognition_v1» и «grz_emirate_recognition_v1»;

Версия 0.0.10
--------------------

* Добавлен классификатор «grz_country_recognition_v1» с поддержкой распознавания страны ГРЗ; 

* Добавлены новые классификаторы распознавания ГРЗ: 

    * eu_recognition_v1 – Евросоюз 

    * rus_spec_recognition_v1 – спецтранспорт 

    *  grz_bel_ukr_kzh_recognition_v1 – Беларусь, Украина, Казахстан 

* Добавлен «grz_all_countries» – классификатор используется для определения принадлежности ГРЗ стране, а затем запускает классификатор распознавания для соответствующей страны; 

* Добавлена поддержка AVX2 для всех моделей распознавания, улучшая показатель скорости распознавания в 2.5 раза. 

Версия 0.0.9
-------------

* Добавлен классификатор «vehicle_color»; 

* Обновлен классификатор «car_brand_model». Реализована поддержка 800 моделей ТС; 

* Добавлен классификатор «vehicle_type»; 

* Добавлен классификатор «vehicle_descriptor»;  

* Реализована поддержка выбора сети для каждого из классификаторов в «modelRunner.cfg»; 

* Добавлены сети с поддержкой AVX2 для некоторых классификаторов; 

* Реализовано возвращение ошибки при запросе результата работы классификатора в случае, если для него не найдена сеть; 

* Удален классификатор «grz_ai_recognition»; 

* Результат работы классификатора «grz_ai_recognition_v2» может быть отображен. Для настройки используется флаг «useLatinCharacters» в файле «modelRunner.cfg». 

Версия 0.0.8
-------------

* Добавлена запись логов «Event manager» для результатов распознавания; 

* Добавлена поддержка запроса /bulk_classify; 

* Добавлен классификатор «car_brand_model». Реализована поддержка 400 моделей ТС. Добавлен порог «Car brand model» в «modelRunner.cfg». Если результат распознавания ниже порогового, то возвращается background; 

* Добавлен флаг «useLatinCharacters» в «modelRunner.cfg». Результаты для классификаторов «grz_ai_recognition» и «marka_taxi_mt» будут отображаться латинскими или кириллическими символами; 

* Добавлен классификатор «grz_ai_recognition_v2». Результат распознавания отображается латинскими символами. 

Версия 0.0.7
----------------

* Доработан механизм GIL locking; 
* Увеличено максимальное время обработки изображений в Python; 
* Устранены проблемы с корректной остановкой сервиса. 

Версия 0.0.6
----------------

* Добавлен классификатор «solid_line_intersection», который определяет если автомобиль нарушил запрет на пересечение сплошной линии. 

Версия 0.0.5
---------------

* Доработан механизм кроппирования (обрезки) кадров; 

* Реализована конвертация из BGR в RGB для классификатора «speed_bad_good_spec». 

Версия 0.0.4
---------------

* Доработана маршрутизация в API.

Версия 0.0.3
---------------

* Добавлена документация.

Версия 0.0.2
----------------

* Добавлено поле «length_scores» в классификатор «grz_ai_recognition»; 

* Улучшена постобработка для «speed_bad_good_spec». 

Версия 0.0.1
---------------

* Добавлен классификатор «marka_taxi_mt» – возвращает марку ТС и его принадлежность к маршрутному транспорту;

* Добавлен классификатор «pmt_grz_quality» – оценивает качество изображения ГРЗ; 

* Добавлен классификатор «grz_ai_recognition» – используется для Добавлен классификатор распознавания ГРЗ; 

* Добавлен классификатор «speed_bad_good_spec» – определяет код нарушения скоростного режима; 

* Добавлен классификатор «pmt_bad_photo» – оценивает качество изображения ТС; 

* Добавлен классификатор «ts_bcd_type» – определяет категорию ТС. 

